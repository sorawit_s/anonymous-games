var MenuLayer = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( res.MenuBGMusic_mp3, true );//play sound
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );  
        this.backgroundmenu = new BackGroundMenu();
        this.backgroundmenu.setPosition( new cc.Point( 410, 300 ) );
        this.addChild( this.backgroundmenu );
	    this.backgroundmenu.scheduleUpdate();
        this.addKeyboardHandlers();
        this.knifecurser = new KnifeCurser();
        this.knifecurser.setPosition( new cc.Point( 410, 200 ) );
        this.addChild( this.knifecurser );
	    this.knifecurser.scheduleUpdate();
        this.addKeyboardHandlers();
        
        this.checkSelect = 1;
        this.isPlaySelectSound = false;
        this.isPlayCurserSound = false;
        
	    return true;
    },
    
    update: function() {
       
    },
    
    onKeyDown: function( e ) {
        switch(e)
        {
            case cc.KEY.enter:  
                if(!this.isPlaySelectSound)
                {
                    cc.audioEngine.stopAllEffects();
                    this.soundEffect(2);
                    this.isPlaySelectSound = true;
                    if(this.checkSelect==1)
                        this.changeScene(1);
                    else
                        this.changeScene(2);
                }
                break;
            case cc.KEY.up:
                this.knifecurser.setPosition( new cc.Point( 410, 200 ) );
                cc.audioEngine.stopAllEffects();
                this.soundEffect(1);
                this.checkSelect = 1;
                break;
            case cc.KEY.down:
                this.knifecurser.setPosition( new cc.Point( 410, 110 ) );
                cc.audioEngine.stopAllEffects();
                this.soundEffect(1);
                this.checkSelect = 2;
                break;
        }
    },
    
    onKeyUp: function( e ) {
        switch(e)
        {
            case cc.KEY.up:
                break;
            case cc.KEY.down:
                break;
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            },
            onKeyReleased: function( e ) {
                 self.onKeyUp( e );
            }
        }, this);
    },
    
    soundEffect: function( select ){
        var number = select;
        switch(number){
            case 1: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/cell_phone_back_cover_remove.mp3' );
                break;
            case 2:
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
                break;
        }
    },
    
    changeScene : function( number ){
        if(number==1)
            //load resources
            cc.LoaderScene.preload(OpenStoryScene_resources, function () {
                cc.audioEngine.stopMusic();
                cc.audioEngine.stopAllEffects();
	            cc.director.runScene(new cc.TransitionFade(1, new BeginStoryScene()));
            }, this);
        else
            cc.director.runScene(new cc.TransitionFade(1, new HowToPlayScene()));
    }
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new LogoScene();
        layer.init();
        this.addChild( layer );
    }
});

var BeginStoryScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new OpenStoryScene();
        layer.init();
        this.addChild( layer );
    }
});

var HowToPlayScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new HowtoplayLayer();
        layer.init();
        this.addChild( layer );
    }
});