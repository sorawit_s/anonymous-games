var OpenStoryScene = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( res.OpenStoryBGMusic_mp3 );
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.background = new BackGround();
        this.goddess = new Goddess(1);
        this.textbox = new TextBox();
        this.initAddChildGameLayer();
        this.gameLayerVariable();
        
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
	    return true;
    },
    update: function() {
        var posMainCharactorScene = this.mainCharactorScene.getPosition();
        if( !this.isAddTextBox && (posMainCharactorScene.x==300) )
        {
            this.addChild( this.textbox );
            this.isAddTextBox = true;
        }
        if(!this.isShowMessage&&this.isAddTextBox){
            this.messageControl();
            this.isShowMessage = true;
        }
    },
    initAddChildGameLayer: function(){
	    this.background.setPosition( new cc.Point( 400, 300 ) );
	    this.goddess.setPosition( new cc.Point( 650, 200 ) );
	    this.textbox.setPosition( new cc.Point( 400, 480 ) );
        this.mainCharactorScene = new MainCharactorScene();
        this.mainCharactorScene.setPosition( new cc.Point( -100, 70 ) );
        this.addChild( this.background );
        this.addChild( this.goddess );
        this.addChild( this.mainCharactorScene );
	    this.background.scheduleUpdate();
        this.goddess.scheduleUpdate();
        this.textbox.scheduleUpdate();
        this.mainCharactorScene.scheduleUpdate();
    },
    gameLayerVariable: function(){
        this.mainCharactorDirection = 'right';
        this.knifePosition = 0;       
        this.isShowMessage = false;
        this.orderOfSentense = 1;
        this.isAddTextBox = false;
    },
    onKeyDown: function( e ) {
        if(e == cc.KEY.enter)
        {
            this.isShowMessage = false;
        }
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            }
        }, this);
    },
    messageControl: function(){
        if(this.orderOfSentense<21)
        {
            this.removeChild(this.labelText);
            this.removeChild(this.labelWhoSpeak);
            this.showMessage(this.orderOfSentense);
            this.orderOfSentense++;
        }
        else
        {
            this.removeChild(this.labelText);
            this.removeChild(this.labelWhoSpeak);
            this.removeChild(this.textbox);
            this.isShowMessage = true;
            this.isTalkFinish = true;
            this.changeToFirstStage(); // changeToFirstStageScene
        }
    },
    showMessage: function(orderOfSentense) {
        var text = this.textbox.openingText(orderOfSentense);
        this.labelText = new cc.LabelTTF( text , "Helvetica", 20);
        this.labelText.setColor(cc.color(0,0,0));//black color
        this.labelText.setPosition(cc.p(400, 520));
        this.soundEffect();
        this.addChild(this.labelText);
        var whospeak = this.textbox.whoSpeak(orderOfSentense);
        this.labelWhoSpeak = new cc.LabelTTF( whospeak , "Helvetica", 20);
        this.labelWhoSpeak.setColor(cc.color(0,0,0));//black color
        this.labelWhoSpeak.setPosition(cc.p(300, 420));
        this.addChild(this.labelWhoSpeak);
    },
    soundEffect: function( ){
        cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
    },
    changeToFirstStage: function(){
        cc.audioEngine.stopAllEffects();
        var delay=1500;                         //delay 1.5 seconds
        setTimeout(function(){
            cc.director.runScene(new cc.TransitionFade(1, new PlayFirstStageOpenScene()));
        },delay);
    }
    
});

var PlayFirstStageOpenScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new FirstStageOpenScene();
        layer.init();
        this.addChild( layer );
    }
});