var FirstStageLayer = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( res.StageOneBGMusic_mp3, true );//play sound
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.initAddChildFirstStageLayer();
        this.FirstStageLayerVariable();
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        this.hitpointbar = new HitpointBar();
        this.hitpointbar.setPosition( new cc.Point( 135 , 555 ) );
        this.addChild(this.hitpointbar);
        this.scheduleUpdate();
        
        this.isHit = false;
        this.isPause = false;
        this.isKnifeThrow = false;
        this.isKnife2Throw = false;
        this.isKnife3Throw = false;
        this.isHasMonster = false;
        this.isKnife4Appear = false;
        this.numberOfMonsterAppear = 0;
        this.isCollectKnife4 = false;
        this.isMainCharactorAppear = true;
        
        this.hitpoint = 29;
        this.hitpointBeforeChange = 29;
        this.arrayBlood = new Array(29);
        
        this.createMonster();
        this.createHitpoint(this.hitpoint);
        this.createItemBar();
        this.hitpointChange = false;
        
	    return true;
    },
    
    update: function() {
        var pos = this.mainCharactor.getPosition();
        this.isMainCharactorAppear = true;
        
        if(this.hitpoint<=0)
        {
            this.removeChild(this.mainCharactor);
            cc.audioEngine.stopAllEffects();
            var delay=4000;  //่delay 4 seconds
            setTimeout(function(){
                cc.audioEngine.stopAllEffects();
                cc.director.runScene(new GameOver());
            },delay);
        }
        
        if(this.hitpointChange)                 //bloodSystem
        {
            for(var i=0; i<this.hitpointBeforeChange ;i++)
            {
                this.removeChild(this.arrayBlood[i]);
            }
            this.createHitpoint(this.hitpoint);
            this.hitpointChange = false;
        }
        if( pos.x <= 40) 
        {
            //console.log("1-1");
            if(this.background.getPosition().x <= 1150)
            {
            //console.log("2-1");
                if(this.mainCharactor.isMovingLeft)
                {
                    //console.log("3-1");
                    this.mainCharactor.setPosition( pos.x+4 , pos.y );
                    this.background.setPosition( this.background.getPosition().x+3 , this.background.getPosition().y );
                    this.littleRhyno.bgScrollActive("left");
                }
            }
        }
        
        if(  pos.x > screenWidth/2 )
        {
            //console.log("1");
            if(this.background.getPosition().x >= screenWidth - 1200)
            {
                //console.log("2");
                if(this.mainCharactor.isMovingRight)
                {
                    //console.log("3");
                    this.mainCharactor.setPosition( pos.x-4 , pos.y );
                    this.background.setPosition( this.background.getPosition().x-3 , this.background.getPosition().y );
                    this.littleRhyno.bgScrollActive("right");
                }
            }
        }
        
        if(this.checkKnifeIsRemove()!=0)
        {
            switch(this.checkKnifeIsRemove())
            {
                case 1:
                        this.isKnifeThrow = false;
                        this.knife = null;
                        break;
                case 2: 
                        this.isKnife2Throw = false;
                        this.knife2 = null;
                        break;
                case 3:
                        this.isKnife3Throw = false;
                        this.knife3 = null;
                        break;
            }
        }
        
	    if( this.knifeHitRhyno()!=0 && !this.isHit) 
        {
           this.soundEffect(3);
           this.removeChild(this.littleRhyno);
           switch(this.knifeHitRhyno())
           {
               case 1:
                       this.knife.removeKnife();
                       this.knife = null;
                       this.isKnifeThrow = false;
                       break;
               case 2:
                       this.knife2.removeKnife();
                       this.knife2 = null;
                       this.isKnife2Throw = false;
                       break;
               case 3: 
                       this.knife3.removeKnife();
                       this.knife3 = null;
                       this.isKnife3Throw = false;
                       break;
           }
           this.isHit = true;
           this.isHasMonster = false;
	    }
        
        if(!this.isHasMonster&&this.numberOfMonsterAppear<FirstStageLayer.NUMBEROFMONSTER)
        {
           this.isHit = false;
           this.createMonster();
        }
        
        if(this.checkRhynoHit()&&this.isMainCharactorAppear&&this.isHasMonster) //check monster Hit
        {
            var posMainChar = this.mainCharactor.getPosition();
            var posMonster = this.littleRhyno.getPosition();
            this.hitpoint = this.hitpoint-3;
            this.hitpointChange = true;
            cc.audioEngine.stopAllEffects();
            this.soundEffect(2);
            this.isMainCharactorAppear = false;
            if(posMainChar.x <= posMonster.x)
            {
                if(posMainChar.x>230)
                    this.mainCharactor.setPosition(posMainChar.x - 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x - (posMainChar.x- 80), posMainChar.y );
            }
            else
            {
                if(posMainChar.x<610)
                    this.mainCharactor.setPosition(posMainChar.x + 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x + ( 760 - posMainChar.x ), posMainChar.y );
            }
        }
        
        if(this.numberOfMonsterAppear==FirstStageLayer.NUMBEROFMONSTER&&!this.isHasMonster)
        {
            if(this.background.getPosition().x <= screenWidth - 1200)
            {
                if(!this.isKnife4Appear)  
                    this.createKnife4(200,100);
            }
        }
        
        if(this.checkGetKnife()&&!this.isCollectKnife4)            // Finish this Stage
        {
            this.soundEffect(1);
            this.knife4.removeKnife();
            this.isCollectKnife4 = true;
            this.createKnife4(600,550);
            var delay=3000;                     //่delay 3 seconds
            setTimeout(function(){
                //load resources
                cc.LoaderScene.preload(OpenStoryScene_resources, function () {
                    cc.audioEngine.stopMusic();
                    cc.audioEngine.stopAllEffects();
	               cc.director.runScene(new cc.TransitionFade(1, new FirstStageClear()));
                }, this);
            },delay);
        }
    },
    
    initAddChildFirstStageLayer: function(){
        this.background = new BackGroundFirstStage();
        this.mainCharactor = new MainCharactor();
	    this.background.setPosition( new cc.Point( 1200, 300 ) );
        this.mainCharactor.setPosition( new cc.Point( 120, 70 ) );
        this.addChild( this.background );
 	    this.addChild( this.mainCharactor );
	    this.background.scheduleUpdate();
        this.mainCharactor.scheduleUpdate();
    },
    
    FirstStageLayerVariable: function(){
        this.knifePosition = 0;    
        this.mainCharactorDirection = 'right';
    },
    
    onKeyDown: function( e ) {
        switch(e)
        {
            case cc.KEY.space:  
                this.mainCharactor.jump(); 
                break;
            case cc.KEY.left:
                this.mainCharactor.move = true;
                this.mainCharactorDirection = 'left';
                this.mainCharactor.moveLeft();
                break;
            case cc.KEY.right:
                this.mainCharactor.move = true;
                this.mainCharactorDirection = 'right';
                this.mainCharactor.moveRight();
                break;
            case cc.KEY.z:
                if(!this.isKnifeThrow)
                {
                    this.createKnife();
                    this.soundEffect(4);
                    this.isKnifeThrow = true;
                }
                break;
            case cc.KEY.x:
                if(!this.isKnife2Throw)
                {
                    this.createKnife2();
                    this.soundEffect(4);
                    this.isKnife2Throw = true;
                }
                break;
            case cc.KEY.c:
                if(!this.isKnife3Throw)
                {
                    this.createKnife3();
                    this.soundEffect(4);
                    this.isKnife3Throw = true;
                }
                break;
            case cc.KEY.p: case cc.KEY.escape:
                if(!this.isPause){
                    this.soundEffect(1);
                    this.textPause(1);
                    cc.director.pause();
                    this.isPause = true;
                }
                else
                {
                    this.soundEffect(1);
                    this.textPause(2);
                    cc.director.resume();
                    this.isPause = false;
                }
                break;
        }
    },
    
    onKeyUp: function( e ) {
        switch(e)
        {
            case cc.KEY.left:
                this.mainCharactor.move = false;
                this.mainCharactor.setToDefault();
                break;
            case cc.KEY.right:
                this.mainCharactor.move = false;
                this.mainCharactor.setToDefault();
                break;
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            },
            onKeyReleased: function( e ) {
                 self.onKeyUp( e );
            }
        }, this);
    },
    
    createKnife: function(){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife = new Knife('right');
	        this.knife.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife = new Knife('left');
	        this.knife.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife );
	    this.knife.scheduleUpdate();
    },
    
    createKnife2: function() {
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife2 = new Knife2('right');
	        this.knife2.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife2 = new Knife2('left');
	        this.knife2.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife2 );
	    this.knife2.scheduleUpdate();
    },
    
    createKnife3: function() {
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife3 = new Knife3('right');
	        this.knife3.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife3 = new Knife3('left');
	        this.knife3.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife3 );
	    this.knife3.scheduleUpdate();
    },
    
    textPause: function( condition ){
        var number = condition;
        if(number==1)
        {
            this.labelWhoSpeak = new cc.LabelTTF( "PAUSE" , "Helvetica", 30);
            this.labelWhoSpeak.setColor(cc.color(255,255,255));//white color
            this.labelWhoSpeak.setPosition(cc.p(400, 300));
            this.addChild(this.labelWhoSpeak);
        }
        else if(number==2)
        {
            this.removeChild(this.labelWhoSpeak);
        }
    },
    
	checkCollision: function( obj1 , obj2 ) {
		if( obj1 != null && obj2 != null ) {
			var obj1Pos = obj1.getPosition();
			var obj2Pos = obj2.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 35 && distanceY < 35 );
		}
	},
    
    knifeHitRhyno: function(){
        if(this.checkCollision(this.knife,this.littleRhyno))
            return 1;
        else if(this.checkCollision(this.knife2,this.littleRhyno))
            return 2;
        else if(this.checkCollision(this.knife3,this.littleRhyno))
            return 3;
        return 0;
    },
    
    checkKnifeIsRemove: function(){
        if(this.isKnifeThrow)
        {
            if(this.knife.checkRemoveKnife())
                return 1;
        }
        else if(this.isKnife2Throw)
        {
            if(this.knife2.checkRemoveKnife())
                return 2;
        }
        else if(this.isKnife3Throw)
        {
            if(this.knife3.checkRemoveKnife())
                return 3;
        }
        return 0;
    },
    
    checkRhynoHit: function(){
			var obj1Pos = this.mainCharactor.getPosition();
			var obj2Pos = this.littleRhyno.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 38  && distanceY < 38 );
    },
    
    createMonster: function(){
        this.littleRhyno = new LittleRhyno();
        this.littleRhyno.setPosition( new cc.Point( 900, 75 ) );
        this.addChild(this.littleRhyno);
        this.littleRhyno.scheduleUpdate();
        this.isHasMonster = true;
        this.numberOfMonsterAppear++;
    },
    
    createKnife4: function(x,y){
        this.knife4 = new Knife4();
        this.knife4.notMove();
        this.knife4.setPosition( new cc.Point( x, y ) );
        this.addChild(this.knife4);
        this.knife4.scheduleUpdate();
        this.isKnife4Appear = true;
    },
    
    checkGetKnife: function(){
        if(this.isKnife4Appear)
        {
            var obj1Pos = this.mainCharactor.getPosition();
			var obj2Pos = this.knife4.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 30 && distanceY < 80 );
        }
        return false;
    },
    
    createHitpoint: function(number){
        this.createBlood(number);
    },
    
    createBlood: function(totalHitpoint){
        if(totalHitpoint>0)
        {
            for(var i=0; i<totalHitpoint ;i++)
            {
                this.arrayBlood[i] = new Blood();
                this.arrayBlood[i].setPosition( new cc.Point( 110.75 + i*5 , 535.5 ) );
                this.addChild(this.arrayBlood[i]);
                this.arrayBlood[i].scheduleUpdate();
            }
        }
    },
    
    createItemBar: function(){
        this.knife = new Knife();
        this.knife.notMove();
        this.knife.setPosition( new cc.Point( 750, 550 ) );
        this.addChild(this.knife);
        this.knife.scheduleUpdate();
        this.knife2 = new Knife2();
        this.knife2.notMove();
        this.knife2.setPosition( new cc.Point( 700, 550 ) );
        this.addChild(this.knife2);
        this.knife2.scheduleUpdate();
        this.knife3 = new Knife3();
        this.knife3.notMove();
        this.knife3.setPosition( new cc.Point( 650, 550 ) );
        this.addChild(this.knife3);
        this.knife3.scheduleUpdate();
    },
    
    soundEffect: function( select ){
        var number = select;
        switch(number){
            case 1: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
                break;
            case 2: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/hit.mp3' );
                break;
            case 3: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/hit2.mp3' );
                break;
            case 4: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/throwKnife.mp3' );
                break;
        }
    }
});

FirstStageLayer.NUMBEROFMONSTER = 10;

var FirstStageClear = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new FirstStageClearScene();
        layer.init();
        this.addChild( layer );
    }
});

var GameOver = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameOverScene();
        layer.init();
        this.addChild( layer );
    }
});