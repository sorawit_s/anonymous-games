var Goddess = cc.Sprite.extend({
    ctor: function(number) {
        this._super();
        this.initWithFile( 'res/images/goddess-R.png' );
        this.direction_y = 0;
        this.direction_x = 0;
        this.setFlippedX(true);
        this.isMoveTop = false;
        this.state = number;
    },
    update: function(){
        var pos = this.getPosition();
        if(this.state==1)
        {
            if(pos.y<=203&&!(this.isMoveTop))
            {
                this.setPosition( new cc.Point( pos.x , pos.y+0.125) );
            }
            else
            {
                this.isMoveTop = true;
                this.setPosition( new cc.Point( pos.x , pos.y-0.125) );
                if(pos.y<=197)
                    this.isMoveTop = false;
            }
        }
        else
        {
            this.setPosition( new cc.Point( pos.x , pos.y-0.75) );
            if(pos.y<=197)
            {
                this.state = 1;
            }
        }
    }
});