var MainCharactorUseForce = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/MainCharactor(Special)-R-1.png' );
        this.movingAction = this.createAnimationAction();
        this.mainCharactorUseForceVariable();
    },
    
    update: function( dt ) {
        this.moveRight();
    },
    
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor(Special)-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/MainCharactor(Special)-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor(Special)-R-3.png' );
	    animation.setDelayPerUnit( 0.4 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    mainCharactorUseForceVariable: function(){
        this.isMovingLeft = false;
        this.isMovingRight = false;
        this.move = false;
    },
    
    moveLeft: function(){
        this.setFlippedX(true);
        if(this.isMovingLeft==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
        }
    },
    
    moveRight: function() {
        this.setFlippedX(false);
        if(this.isMovingRight==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
        }
    },
    
    setToDefault: function(){
        if(this.isMovingLeft)
        {
            this.isMovingLeft=false;
            this.stopAction( this.movingAction );
        }
        if(this.isMovingRight)
        {
            this.isMovingRight=false;
            this.stopAction( this.movingAction );
        }
        this.direction_x = 0;
    }
});