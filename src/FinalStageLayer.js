var FinalStageLayer = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.playMusic( res.StageFinalBGMusic_mp3, true );//play sound
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.initAddChildFinalStageLayer();
        this.finalStageLayerVariable();
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        this.hitpointbar = new HitpointBar();
        this.hitpointbar.setPosition( new cc.Point( 135 , 555 ) );
        this.addChild(this.hitpointbar);
        this.scheduleUpdate();
        
        this.boss = new Venon();
        this.boss.setPosition( new cc.Point( 650, 100 ) );
        this.addChild(this.boss);
        this.boss.scheduleUpdate();
        this.boss.moveLeft();
        
        this.isPause = false;
        this.isKnifeThrow = false;
        this.isKnife2Throw = false;
        this.isKnife3Throw = false;
        this.isKnife4Throw = false;
        this.isKnife5Throw = false;
        this.isKnife6Throw = false;
        this.isKnife7Throw = false;
        this.isMainCharactorAppear = true;
        
        this.fireballFirstTime = true;
        this.fireball = null;
        this.hasFireball = false;
        this.fireballDirection = 'left';
        
        this.hitpoint = 29;
        this.hitpointBeforeChange = 29;
        this.arrayBlood = new Array(29);
        
        this.createHitpoint(this.hitpoint);
        this.createItemBar();
        this.hitpointChange = false;
        
        this.countTime = false;
        
        ////////////////
        this.start = new Date();
        ////////////////
        
        this.createFireball();
        
	    return true;
    },
    
    update: function() {
        this.isMainCharactorAppear = true;
        this.countTime = false;
        
        this.Timer(); //check timer 3 second.
        
        if(this.hitpoint<=0)
        {
            this.removeChild(this.mainCharactor);
//            var delay=4000;  //่delay 4 seconds
//            setTimeout(function(){
                cc.audioEngine.stopAllEffects();
                cc.director.runScene(new GameOver());
//            },delay);
        }
        
        if(this.hitpointChange)                 //bloodSystem
        {
            for(var i=0; i<this.hitpointBeforeChange ;i++)
            {
                this.removeChild(this.arrayBlood[i]);
            }
            this.createHitpoint(this.hitpoint);
            this.hitpointChange = false;
        }
        
        if(this.countTime)
        {
            this.boss.move();
            this.createFireball();
        }
        
        if(!this.fireballFirstTime&&this.hasFireball)
        {
            if(this.fireball.isRemove)
                this.hasFireball = false;
        }
        
        if(this.checkKnifeIsRemove()!=0)
        {
            switch(this.checkKnifeIsRemove())
            {
                case 1:
                        this.isKnifeThrow = false;
                        this.knife = null;
                        break;
                case 2: 
                        this.isKnife2Throw = false;
                        this.knife2 = null;
                        break;
                case 3:
                        this.isKnife3Throw = false;
                        this.knife3 = null;
                        break;
                case 4:
                        this.isKnife4Throw = false;
                        this.knife4 = null;
                        break;
                case 5:
                        this.isKnife5Throw = false;
                        this.knife5 = null;
                        break;
                case 6:
                        this.isKnife6Throw = false;
                        this.knife6 = null;
                        break;
                case 7:
                        this.isKnife7Throw = false;
                        this.knife7 = null;
                        break;
            }
        }
        
	    if( this.knifeHitBoss()!=0) 
        {
           this.soundEffect(3);
           this.boss.knifehit();
           switch(this.knifeHitBoss())
           {
               case 1:
                       this.knife.removeKnife();
                       this.knife = null;
                       this.isKnifeThrow = false;
                       break;
               case 2:
                       this.knife2.removeKnife();
                       this.knife2 = null;
                       this.isKnife2Throw = false;
                       break;
               case 3: 
                       this.knife3.removeKnife();
                       this.knife3 = null;
                       this.isKnife3Throw = false;
                       break;
               case 4: 
                       this.knife4.removeKnife();
                       this.knife4 = null;
                       this.isKnife4Throw = false;
                       break;
               case 5: 
                       this.knife5.removeKnife();
                       this.knife5 = null;
                       this.isKnife5Throw = false;
                       break;
               case 6: 
                       this.knife6.removeKnife();
                       this.knife6 = null;
                       this.isKnife6Throw = false;
                       break;
               case 7: 
                       this.knife7.removeKnife();
                       this.knife7 = null;
                       this.isKnife7Throw = false;
                       break;
           }
	    }
        
        if( this.checkFireballHit() ) ////////////////////////////
        {
	       var posMainChar = this.mainCharactor.getPosition();
           var posFireball = this.fireball.getPosition();
           this.soundEffect(2);
           this.hitpoint = this.hitpoint-5;
           this.hitpointChange = true;
           if(posMainChar.x <= posFireball.x)
            {
                if(posMainChar.x>230)
                    this.mainCharactor.setPosition(posMainChar.x - 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x - (posMainChar.x- 80), posMainChar.y );
            }
            else
            {
                if(posMainChar.x<610)
                    this.mainCharactor.setPosition(posMainChar.x + 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x + ( 760 - posMainChar.x ), posMainChar.y );
            }
            this.fireball.removeFireball();
            this.fireball = null;
            this.hasFireball = false;
	    }
        
        if(this.checkBossHit()&&this.isMainCharactorAppear) //check monster Hit
        {
            var posMainChar = this.mainCharactor.getPosition();
            var posBoss = this.boss.getPosition();
            console.log("boss hit");
            this.hitpoint = this.hitpoint-3;
            this.hitpointChange = true;
            cc.audioEngine.stopAllEffects();
            this.soundEffect(2);
            this.isMainCharactorAppear = false;
            if(posMainChar.x <= posBoss.x)
            {
                if(posMainChar.x>230)
                    this.mainCharactor.setPosition(posMainChar.x - 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x - (posMainChar.x- 80), posMainChar.y );
            }
            else
            {
                if(posMainChar.x<610)
                    this.mainCharactor.setPosition(posMainChar.x + 150, posMainChar.y );
                else
                    this.mainCharactor.setPosition(posMainChar.x + ( 760 - posMainChar.x ), posMainChar.y );
            }
        }
        
        if(this.boss.hitpoint<=0)            // Finish this Stage
        {
            cc.audioEngine.stopAllEffects();
            cc.director.runScene(new PlayCloseScene());
        }
        
    },
    
    initAddChildFinalStageLayer: function(){
        this.background = new BackGroundFinalStage();
        this.mainCharactor = new MainCharactor();
	    this.background.setPosition( new cc.Point( 400, 300 ) );
        this.mainCharactor.setPosition( new cc.Point( 120, 70 ) );
        this.addChild( this.background );
 	    this.addChild( this.mainCharactor );
	    this.background.scheduleUpdate();
        this.mainCharactor.scheduleUpdate();
    },
    
    finalStageLayerVariable: function(){
        this.knifePosition = 0;
        this.mainCharactorDirection = 'right';
    },
    
    onKeyDown: function( e ) {
        switch(e)
        {
            case cc.KEY.space:  
                this.mainCharactor.jump(); 
                break;
            case cc.KEY.left:
                this.mainCharactor.move = true;
                this.mainCharactorDirection = 'left';
                this.mainCharactor.moveLeft();
                break;
            case cc.KEY.right:
                this.mainCharactor.move = true;
                this.mainCharactorDirection = 'right';
                this.mainCharactor.moveRight();
                break;
            case cc.KEY.z:
                if(!this.isKnifeThrow)
                {
                    this.createKnife();
                    this.soundEffect(4);
                    this.isKnifeThrow = true;
                }
                break;
            case cc.KEY.x:
                if(!this.isKnife2Throw)
                {
                    this.createKnife2();
                    this.soundEffect(4);
                    this.isKnife2Throw = true;
                }
                break;
            case cc.KEY.c:
                if(!this.isKnife3Throw)
                {
                    this.createKnife3();
                    this.soundEffect(4);
                    this.isKnife3Throw = true;
                }
                break;
            case cc.KEY.v:
                if(!this.isKnife4Throw)
                {
                    this.createKnife4();
                    this.soundEffect(4);
                    this.isKnife4Throw = true;
                }
                break;
            case cc.KEY.a:
                if(!this.isKnife5Throw)
                {
                    this.createKnife5();
                    this.soundEffect(4);
                    this.isKnife5Throw = true;
                }
                break;
            case cc.KEY.s:
                if(!this.isKnife6Throw)
                {
                    this.createKnife6();
                    this.soundEffect(4);
                    this.isKnife6Throw = true;
                }
                break;
            case cc.KEY.d:
                if(!this.isKnife7Throw)
                {
                    this.createKnife7();
                    this.soundEffect(4);
                    this.isKnife7Throw = true;
                }
                break;
            case cc.KEY.p: case cc.KEY.escape :
                if(!this.isPause){
                    this.soundEffect(1);
                    this.textPause(1);
                    cc.director.pause();
                    this.isPause = true;
                }
                else
                {
                    this.soundEffect(1);
                    this.textPause(2);
                    cc.director.resume();
                    this.isPause = false;
                    this.start = new Date();
                }
                break;
        }
    },
    
    onKeyUp: function( e ) {
        switch(e)
        {
            case cc.KEY.left:
                this.mainCharactor.move = false;
                this.mainCharactor.setToDefault();
                break;
            case cc.KEY.right:
                this.mainCharactor.move = false;
                this.mainCharactor.setToDefault();
                break;
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            },
            onKeyReleased: function( e ) {
                 self.onKeyUp( e );
            }
        }, this);
    },
    
    createKnife: function(){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife = new Knife('right');
	        this.knife.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife = new Knife('left');
	        this.knife.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife );
	    this.knife.scheduleUpdate();
    },
    
    createKnife2: function() {
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife2 = new Knife2('right');
	        this.knife2.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife2 = new Knife2('left');
	        this.knife2.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife2 );
	    this.knife2.scheduleUpdate();
    },
    
    createKnife3: function() {
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife3 = new Knife3('right');
	        this.knife3.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife3 = new Knife3('left');
	        this.knife3.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife3 );
	    this.knife3.scheduleUpdate();
    },
    
    createKnife4: function(x,y){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife4 = new Knife4('right');
	        this.knife4.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife4 = new Knife4('left');
	        this.knife4.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife4 );
	    this.knife4.scheduleUpdate();
    },
    
    createKnife5: function(x,y){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife5 = new Knife5('right');
	        this.knife5.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife5 = new Knife5('left');
	        this.knife5.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife5 );
	    this.knife5.scheduleUpdate();
    },
    
    createKnife6: function(x,y){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife6 = new Knife6('right');
	        this.knife6.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife6 = new Knife6('left');
	        this.knife6.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife6 );
	    this.knife6.scheduleUpdate();
    },
    
    createKnife7: function(x,y){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife7 = new Knife7('right');
	        this.knife7.setPosition( new cc.Point( pos.x+40, pos.y-10 ) );
        }
        else    //left
        {
            this.knife7 = new Knife7('left');
	        this.knife7.setPosition( new cc.Point( pos.x-40, pos.y-10 ) );
        }
        this.addChild( this.knife7 );
	    this.knife7.scheduleUpdate();
    },
    
    textPause: function( condition ){
        var number = condition;
        if(number==1)
        {
            this.labelWhoSpeak = new cc.LabelTTF( "PAUSE" , "Helvetica", 30);
            this.labelWhoSpeak.setColor(cc.color(255,255,255));//white color
            this.labelWhoSpeak.setPosition(cc.p(400, 300));
            this.addChild(this.labelWhoSpeak);
        }
        else if(number==2)
        {
            this.removeChild(this.labelWhoSpeak);
        }
    },
    
	checkCollision: function( obj1 , obj2 ) {
		if( obj1 != null && obj2 != null ) {
			var obj1Pos = obj1.getPosition();
			var obj2Pos = obj2.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 60 && distanceY < 90 );
		}
	},
    
    knifeHitBoss: function(){
        if(this.checkCollision(this.knife,this.boss))
            return 1;
        else if(this.checkCollision(this.knife2,this.boss))
            return 2;
        else if(this.checkCollision(this.knife3,this.boss))
            return 3;
        else if(this.checkCollision(this.knife4,this.boss))
            return 4;
        else if(this.checkCollision(this.knife5,this.boss))
            return 5;
        else if(this.checkCollision(this.knife6,this.boss))
            return 6;
        else if(this.checkCollision(this.knife7,this.boss))
            return 7;
        return 0;
    },
    
    checkKnifeIsRemove: function(){
        if(this.isKnifeThrow)
        {
            if(this.knife.checkRemoveKnife())
                return 1;
        }
        else if(this.isKnife2Throw)
        {
            if(this.knife2.checkRemoveKnife())
                return 2;
        }
        else if(this.isKnife3Throw)
        {
            if(this.knife3.checkRemoveKnife())
                return 3;
        }
        else if(this.isKnife4Throw)
        {
            if(this.knife4.checkRemoveKnife())
                return 4;
        }
        else if(this.isKnife5Throw)
        {
            if(this.knife5.checkRemoveKnife())
                return 5;
        }
        else if(this.isKnife6Throw)
        {
            if(this.knife6.checkRemoveKnife())
                return 6;
        }
        else if(this.isKnife7Throw)
        {
            if(this.knife7.checkRemoveKnife())
                return 7;
        }
        return 0;
    },
    
    checkBossHit: function(){
			var obj1Pos = this.mainCharactor.getPosition();
			var obj2Pos = this.boss.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 65  && distanceY < 65 );
    },
    
    createHitpoint: function(number){
        this.createBlood(number);
    },
    
    createBlood: function(totalHitpoint){
        if(totalHitpoint>0)
        {
            for(var i=0; i<totalHitpoint ;i++)
            {
                this.arrayBlood[i] = new Blood();
                this.arrayBlood[i].setPosition( new cc.Point( 110.75 + i*5 , 535.5 ) );
                this.addChild(this.arrayBlood[i]);
                this.arrayBlood[i].scheduleUpdate();
            }
        }
    },
    
    createItemBar: function(){
        this.knife = new Knife();
        this.knife.notMove();
        this.knife.setPosition( new cc.Point( 750, 550 ) );
        this.addChild(this.knife);
        this.knife.scheduleUpdate();
        this.knife2 = new Knife2();
        this.knife2.notMove();
        this.knife2.setPosition( new cc.Point( 700, 550 ) );
        this.addChild(this.knife2);
        this.knife2.scheduleUpdate();
        this.knife3 = new Knife3();
        this.knife3.notMove();
        this.knife3.setPosition( new cc.Point( 650, 550 ) );
        this.addChild(this.knife3);
        this.knife3.scheduleUpdate();
        this.knife4 = new Knife4();
        this.knife4.notMove();
        this.knife4.setPosition( new cc.Point( 600, 550 ) );
        this.addChild(this.knife4);
        this.knife4.scheduleUpdate();
        this.knife5 = new Knife5();
        this.knife5.notMove();
        this.knife5.setPosition( new cc.Point( 550, 550 ) );
        this.addChild(this.knife5);
        this.knife5.scheduleUpdate();
        this.knife6 = new Knife6();
        this.knife6.notMove();
        this.knife6.setPosition( new cc.Point( 500, 550 ) );
        this.addChild(this.knife6);
        this.knife6.scheduleUpdate();
        this.knife7 = new Knife7();
        this.knife7.notMove();
        this.knife7.setPosition( new cc.Point( 450, 550 ) );
        this.addChild(this.knife7);
        this.knife7.scheduleUpdate();
    },
    
    createFireball: function(){
        var pos = this.boss.getPosition();
        if(this.fireballDirection=='right')
        {
            this.fireball = new FireBall('right');
	        this.fireball.setPosition( new cc.Point( 100 + 10, pos.y+5 ) );
            this.fireballDirection = 'left';
        }
        else    //left
        {
            this.fireball = new FireBall('left');
	        this.fireball.setPosition( new cc.Point( 700 - 10, pos.y+5 ) );
            this.fireballDirection = 'right';
        }
        this.addChild( this.fireball );
	    this.fireball.scheduleUpdate();
        this.hasFireball = true;
        this.soundEffect(5);
    },
    
    checkFireballHit: function(){
        if(this.hasFireball)
        {
			var obj1Pos = this.mainCharactor.getPosition();
			var obj2Pos = this.fireball.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 90  && distanceY < 40 );
        }
    },
    
    Timer: function(){
        this.end = new Date();
        if((this.end.getTime() - this.start.getTime() ) >= 3250)
        {
            console.log('Operation took ' + (this.end.getTime() - this.start.getTime()) + ' sec');
            this.start = new Date();
            this.countTime = true;
        }
    },
    
    soundEffect: function( select ){
        
        var number = select;
        switch(number){
            case 1: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
                break;
            case 2: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/hit.mp3' );
                break;
            case 3: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/hit2.mp3' );
                break;
            case 4: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/throwKnife.mp3' );
                break;
            case 5: 
                cc.audioEngine.stopAllEffects();
                cc.audioEngine.playEffect( 'res/sounds/fireBall.mp3' );
                break;
        }
    }
});

var PlayCloseScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new CloseScene();
        layer.init();
        this.addChild( layer );
    }
});