var CreditScene = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( res.CreditSceneBGMusic_mp3 , true );//play sound
        this._super( new cc.Color( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.initAddChildCreditScene();
        this.scheduleUpdate();
        
	    return true;
    },
    update: function() {
        var pos = this.text.getPosition();
        if(pos.y > 1350)
        {
            cc.director.pause();
            var delay=3000;                     //่delay 3 seconds
            setTimeout(function(){
                cc.director.resume();
                cc.director.runScene(new StartScene());
            },delay);
        }
    },
    initAddChildCreditScene: function(){
        this.text = new TextCredit();
        this.text.setPosition(400,-1150);
        this.addChild(this.text);
        this.text.scheduleUpdate();
    }
});