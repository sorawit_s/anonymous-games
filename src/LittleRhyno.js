var LittleRhyno = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Monster/Rhyno-R-1.png' );
        this.LittleRhynoVariable();
        this.speed = LittleRhyno.NORMALSPEED;
        this.bgScroll = false;
    },
    update: function( dt ) {
	    var pos = this.getPosition();
        if(!this.bgScroll)
        {
            this.speed = LittleRhyno.NORMALSPEED;
        }
        if(this.bgScroll)
        {
            if(this.bgDirection=="left")
            {
                if(this.isMovingLeft)
                    this.speed = 5;
                if(this.isMovingRight)
                    this.speed = 8;
            }
            if(this.bgDirection=="right")
            {
                if(this.isMovingLeft)
                    this.speed = 8;
                if(this.isMovingRight)
                    this.speed = 5;
            }
        }
        if(pos.x<770&&!(this.isMoveToRightEdge))
        {
            this.moveRight();
            this.setPosition( new cc.Point( pos.x+this.speed , pos.y) );
        }
        else
        {
            this.moveLeft();
            this.isMoveToRightEdge = true;
            this.setPosition( new cc.Point( pos.x-this.speed , pos.y) );
            if(pos.x<25)
                this.isMoveToRightEdge = false;
        }
        this.bgScroll = false;
    },
    LittleRhynoVariable: function(){
        this.movingAction = this.createAnimationAction();
        this.isMoveToRightEdge = false;
        this.isMovingLeft = false;
        this.isMovingRight = false;
    },
    moveLeft: function(){
        this.setFlippedX(true);
        if(this.isMovingLeft==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
            this.isMovingRight = false;
        }
    },
    moveRight: function() {
        this.setFlippedX(false);
        if(this.isMovingRight==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
            this.isMovingLeft = false;
        }
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Monster/Rhyno-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/Monster/Rhyno-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/Monster/Rhyno-R-3.png' );
	    animation.setDelayPerUnit( 0.5 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    bgScrollActive: function(direction){
        this.bgDirection = direction;
        this.bgScroll = true;
    }
});

LittleRhyno.NORMALSPEED = 8;
