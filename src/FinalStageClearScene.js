var FinalStageClearScene = cc.LayerColor.extend({
    ctor: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( 'res/sounds/ClearFinalStage.mp3' );
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.stageoneBG = new StageFinalClearBG();
        this.stageoneBG.setPosition(400,300);
        this.addChild(this.stageoneBG);
        this.runStageScene();
        this.scheduleUpdate();
    },
    
    update: function()
    {
        
    },
    
    runStageScene: function()
    {
//        var delay=4000;                         //delay 4 seconds
//        setTimeout(function(){
//                cc.director.runScene(new SecondStageOpenScene());
//        },delay);
        cc.director.pause();
    }
    
});

//var SecondStageOpenScene = cc.Scene.extend({
//    onEnter: function() {
//        this._super();
//        var layer = new SecondStageLayer();
//        layer.init();
//        this.addChild( layer );
//    }
//});
