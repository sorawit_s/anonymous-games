var Explosion = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/explosion/explosion-1.png' );
        this.movingAction = this.createAnimationAction();
        this.runAction( this.movingAction );
    },
    update: function( dt ) {
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-3.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-4.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-5.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-6.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-3.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-4.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-5.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-6.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-3.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-4.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-5.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-6.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-3.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-4.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-5.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-6.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-3.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-4.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-5.png' );
        animation.addSpriteFrameWithFile( 'res/images/explosion/explosion-6.png' );
	    animation.setDelayPerUnit( 0.3 );
        this.removeExplosion();
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    removeExplosion: function(){
        this.removeFromParentAndCleanup(true);
    }
});