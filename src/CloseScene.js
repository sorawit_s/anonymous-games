var CloseScene = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.background = new BackGround();
        this.goddess = new Goddess(2);
        this.textbox = new TextBox2();
        this.boss = new Venon();
        this.initAddChildGameLayer();
        this.gameLayerVariable();
        
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
	    return true;
    },
    update: function(dt) {
        if(this.isStartTime)
            this.timer();
        if( !this.isAddTextBox && this.goddess.state == 1)
        {
            this.addChild( this.textbox );
            this.isAddTextBox = true;
        }
        if(!this.isShowMessage&&this.isAddTextBox)
        {
            this.messageControl();
            this.isShowMessage = true;
        }
        if(this.isKnivesMove)
        {
            if(!this.isStartTime)
            {
                ////////////////
                this.start = new Date();
                ////////////////
                this.isStartTime = true;
            }
            if(this.countTime)
            {
                this.knife.setPosition( new cc.Point( this.knife.getPosition().x+3, 60 ) );
                this.knife2.setPosition( new cc.Point( this.knife2.getPosition().x+3, 90 ) );
                this.knife3.setPosition( new cc.Point( this.knife3.getPosition().x+3, 120 ) );
                this.knife4.setPosition( new cc.Point( this.knife4.getPosition().x+3, 150 ) );
                this.knife5.setPosition( new cc.Point( this.knife5.getPosition().x+3, 120 ) );
                this.knife6.setPosition( new cc.Point( this.knife6.getPosition().x+3, 90 ) );
                this.knife7.setPosition( new cc.Point( this.knife7.getPosition().x+3, 60 ) );
                this.isStartTime = false;
            }
            if(this.knife.getPosition().x>=325)
            {
                this.removeChild(this.knife);
                this.removeChild(this.knife2);
                this.removeChild(this.knife3);
                this.removeChild(this.knife4);
                this.removeChild(this.knife5);
                this.removeChild(this.knife6);
                this.removeChild(this.knife7);
                if(!this.isAddEnergyball)
                {
                    this.addChild(this.energyball);
                    this.isAddEnergyball = true;
                    cc.audioEngine.playEffect( 'res/sounds/energyBall.mp3' );
                    var delay=1800;                         //delay 1.8 seconds
                    setTimeout(function(){
                        cc.audioEngine.stopAllEffects();
                        cc.audioEngine.playEffect( 'res/sounds/explosion.mp3' );
                    },delay);
                }
            }
        }
        if(this.energyball.getPosition().x>=650&&this.energyball.isRemove&&!this.isAddExplosion)
        {
            if(!this.isStartTime)
            {
                ////////////////
                this.start = new Date();
                ////////////////
                this.isStartTime = true;
            }
            if(this.countTime)
            {
                this.removeChild(this.boss);
                this.isStartTime = false;
            }
            this.addChild(this.explosion);
            this.addChild(this.theend);
            this.isAddExplosion = true;
            this.changeToCreditStage();
        }
    },
    initAddChildGameLayer: function(){
        this.background = new BackGroundFinalStage();
        this.mainCharactor = new MainCharactor();
        this.mainCharactorUseForce = new MainCharactorUseForce();
        this.knife = new Knife();
        this.knife2 = new Knife2();
        this.knife3 = new Knife3();
        this.knife4 = new Knife4();
        this.knife5 = new Knife5();
        this.knife6 = new Knife6();
        this.knife7 = new Knife7();
        this.energyball = new EnergyBall();
        this.explosion = new Explosion();
        this.theend = new TheEnd();
        this.mainCharactor.setPosition( new cc.Point( 120, 70 ) );
        this.mainCharactorUseForce.setPosition( new cc.Point( 120, 70 ) );
	    this.background.setPosition( new cc.Point( 400, 300 ) );
        this.goddess.setPosition( new cc.Point( 420, 280 ) );
        this.textbox.setPosition( new cc.Point( 400, 480 ) );
        this.boss.setPosition( new cc.Point( 650, 100 ) );
        this.knife.setPosition( new cc.Point( 30, 60 ) );
        this.knife2.setPosition( new cc.Point( 60, 90 ) );
        this.knife3.setPosition( new cc.Point( 90, 120 ) );
        this.knife4.setPosition( new cc.Point( 120, 150 ) );
        this.knife5.setPosition( new cc.Point( 150, 120 ) );
        this.knife6.setPosition( new cc.Point( 180, 90 ) );
        this.knife7.setPosition( new cc.Point( 210, 60 ) );
        this.energyball.setPosition( new cc.Point( 425, 100 ) );
        this.explosion.setPosition( new cc.Point( 700, 110 ) );
        this.theend.setPosition( new cc.Point( 400, 500 ) );
        this.addChild( this.background );
        this.addChild( this.goddess );
        this.addChild( this.boss );
        this.addChild( this.mainCharactor );
	    this.background.scheduleUpdate();
        this.mainCharactor.scheduleUpdate();
        this.goddess.scheduleUpdate();
        this.textbox.scheduleUpdate();
        this.boss.scheduleUpdate();
        this.boss.moveLeft();
        this.mainCharactorUseForce.scheduleUpdate();
        this.energyball.scheduleUpdate();
        this.explosion.scheduleUpdate();
    },
    gameLayerVariable: function(){    
        this.isShowMessage = false;
        this.orderOfSentense = 1;
        this.isAddTextBox = false;
        this.isKnivesMove = false;
        this.isAddEnergyball = false;
        this.isAddExplosion = false;
        this.countTime = false;
        this.isStartTime = false;
        this.isShowMessageFinish = false;
    },
    onKeyDown: function( e ) {
        if(!this.isShowMessageFinish)
        {
            if(e == cc.KEY.enter)
            {
                this.isShowMessage = false;
            }
        }
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            }
        }, this);
    },
    messageControl: function(){
        if(this.orderOfSentense<4)
        {
            this.removeChild(this.labelText);
            this.removeChild(this.labelWhoSpeak);
            this.showMessage(this.orderOfSentense);
            this.orderOfSentense++;
        }
        else
        {
            this.removeChild(this.labelText);
            this.removeChild(this.labelWhoSpeak);
            this.removeChild(this.textbox);
            this.removeChild(this.goddess);
            this.removeChild(this.mainCharactor);
            this.isShowMessage = true;
            this.isTalkFinish = true;
            this.addChild(this.mainCharactorUseForce);
            this.knife.setRotation(-90);
            this.knife2.setRotation(-90);
            this.knife3.setRotation(-90);
            this.knife4.setRotation(-90);
            this.knife5.setRotation(-90);
            this.knife6.setRotation(-90);
            this.knife7.setRotation(-90);
            this.addChild(this.knife);
            this.addChild(this.knife2);
            this.addChild(this.knife3);
            this.addChild(this.knife4);
            this.addChild(this.knife5);
            this.addChild(this.knife6);
            this.addChild(this.knife7);
            this.isKnivesMove = true;
            this.isShowMessageFinish = true;
        }
    },
    showMessage: function(orderOfSentense) {
        var text = this.textbox.openingText(orderOfSentense);
        this.labelText = new cc.LabelTTF( text , "Helvetica", 20);
        this.labelText.setColor(cc.color(0,0,0));//black color
        this.labelText.setPosition(cc.p(400, 510));
        this.soundEffect();
        this.addChild(this.labelText);
        var whospeak = this.textbox.whoSpeak(orderOfSentense);
        this.labelWhoSpeak = new cc.LabelTTF( whospeak , "Helvetica", 20);
        this.labelWhoSpeak.setColor(cc.color(0,0,0));//black color
        this.labelWhoSpeak.setPosition(cc.p(300, 420));
        this.addChild(this.labelWhoSpeak);
    },
    soundEffect: function( ){
        cc.audioEngine.stopAllEffects();
        cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
    },
    changeToCreditStage: function(){
        cc.audioEngine.stopAllEffects();
        var delay=7000;                         //delay 5 seconds
        setTimeout(function(){
            cc.LoaderScene.preload(CreditScene_resources, function () {
                cc.audioEngine.stopMusic();
                cc.audioEngine.stopAllEffects();
	            cc.director.runScene(new cc.TransitionFade(1, new PlayCreditScene()));
            }, this);
        },delay);
    },
    timer: function(){
        this.end = new Date();
        if((this.end.getTime() - this.start.getTime() ) >= 1000)
        {
            this.countTime = true;
            cc.audioEngine.stopAllEffects();
            cc.audioEngine.playEffect( 'res/sounds/throwKnife.mp3' );
        }
    }
    
});

var PlayCreditScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new CreditScene();
        layer.init();
        this.addChild( layer );
    }
});