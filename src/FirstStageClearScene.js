var FirstStageClearScene = cc.LayerColor.extend({
    ctor: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( 'res/sounds/ClearStage.mp3' );
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.stageoneBG = new Stage1ClearBG();
        this.stageoneBG.setPosition(400,300);
        this.addChild(this.stageoneBG);
        this.runStageScene();
        this.scheduleUpdate();
    },
    
    update: function()
    {
        
    },
    
    runStageScene: function()
    {
        var delay=4000;                         //delay 4 seconds
        setTimeout(function(){
            cc.director.runScene(new cc.TransitionFade(1, new SecondStageOpenScene()));
        },delay);
    }
    
});

var SecondStageOpenScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new SecondStageOpenLayer();
        layer.init();
        this.addChild( layer );
    }
});
