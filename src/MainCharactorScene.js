var MainCharactorScene = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/MainCharactor-R-1.png' );
        this.mainCharactorSceneVariable();
    },
    update: function( dt ) {
	    var pos = this.getPosition();
        if ( pos.x <= 300 && !this.isTalkFinish) 
        {
           this.moveRight();
	       this.setPosition( new cc.Point( pos.x+this.direction_x , pos.y) );
	    }
        else 
        {
            if(!this.isMovingRight)
            {
                this.setToDefault();
            }
        }
    },
    mainCharactorSceneVariable: function(){
        this.direction_y = 0;
        this.direction_x = 0;
        this.movingAction = this.createAnimationAction();
        this.isMovingRight = false;
    },
    moveRight: function() {
        this.setFlippedX(false);
        if(!this.isMovingRight)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
        }
	    this.direction_x = MainCharactorScene.DIR.RIGHT;
        this.direction_y = 0;
    },
    setToDefault: function(){
        if(this.isMovingRight)
        {
            this.isMovingRight=true;
            this.stopAction( this.movingAction );
        }
        this.direction_x = 0;
        this.direction_y = 0;
    },
    moveBack: function(){
        this.isTalkFinish = true;
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-3.png' );
	    animation.setDelayPerUnit( 0.5 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    }
});

MainCharactorScene.DIR = {
    LEFT: -2.5,
    RIGHT: 2.5
};