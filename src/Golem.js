var Golem = LittleRhyno.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Monster/golem-R-1.png' );
        this.monkeyVariable();
        
        this.step = 0;
    },
    update: function( dt ) {
	    var pos = this.getPosition();
        this.step += 1;
        if(pos.x<700&&!(this.isMoveToRightEdge))
        {
            this.moveRight();
            this.setPosition( new cc.Point( pos.x+3 , pos.y));
        }
        else
        {
            this.moveLeft();
            this.isMoveToRightEdge = true;
            this.setPosition( new cc.Point( pos.x-3 , pos.y));
            if(pos.x<150)
                this.isMoveToRightEdge = false;
        }      
    },
    moveLeft: function(){
        this.setFlippedX(true);
        if(this.isMovingLeft==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
        }
    },
    moveRight: function() {
        this.setFlippedX(false);
        if(this.isMovingRight==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
        }
    },
    monkeyVariable: function(){
        this.movingAction = this.createAnimationAction();
        this.isMoveToRightEdge = false;
        this.isMovingLeft = false;
        this.isMovingRight = false;
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Monster/golem-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/Monster/golem-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/Monster/golem-R-3.png' );
	    animation.setDelayPerUnit( 0.5 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    }
});