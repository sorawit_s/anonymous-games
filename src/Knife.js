var Knife = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/knives/red.png' );
        this.direction = direction;
        this.isRemove = false;
        this.move = true;
        this.isSpecialMove = false;
    },
    
    update: function( dt ) {
        this.moveKnife();
    },
    
    moveKnife: function( ){
        var pos = this.getPosition();
        if(this.move)
        {
            if(this.direction=='right')
            {
                this.setRotation(-90);
                if(!this.isSpecialMove)
                {
                    this.setPosition( new cc.Point( pos.x+Knife.DIR.RIGHT, pos.y ) );
                    if(this.getPosition().x >= 750)
                    {
                        this.isRemove = true;
                        this.removeKnife();
                    }
                }
                else
                {
                    this.setPosition( new cc.Point( pos.x+Knife.DIR.RIGHT_SPECIAL, pos.y ) );
                    if(this.getPosition().x >= 700)
                    {
                        this.isRemove = true;
                        this.removeKnife();
                    }
                }
            }
            else if(this.direction=='left')//left
            {
                this.setRotation(90);
                if(!this.isSpecialMove)
                {
                    this.setPosition( new cc.Point( pos.x+Knife.DIR.LEFT, pos.y ) );
                    if(this.getPosition().x <= 50)
                    {
                        this.isRemove = true;
                        this.removeKnife();
                    }
                }
                else
                {
                    this.setPosition( new cc.Point( pos.x+Knife.DIR.LEFT_SPECIAL, pos.y ) );
                    if(this.getPosition().x <= 100)
                    {
                        this.isRemove = true;
                        this.removeKnife();
                    }
                }
            }
        }
        else
        {
            this.setPosition( new cc.Point( pos.x, pos.y ) );
        }
    },
    
    removeKnife: function(){
        this.removeFromParentAndCleanup(true);
    },
    
    checkRemoveKnife: function(){
        return this.isRemove;
    },
    
    notMove: function(){
        return this.move = false;
    },
    
    specialMoveActive: function(){
        this.isSpecialMove = true;
    }
});

Knife.DIR = {
    LEFT: -8,
    RIGHT: 8,
    LEFT_SPECIAL: -4,
    RIGHT_SPECIAL: 4
};