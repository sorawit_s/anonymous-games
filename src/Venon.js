var Venon = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Boss/Venon-R-1.png' );
        this.venonSceneVariable();
        this.notMove = false;
    },
    
    update: function( dt ) {
	    var pos = this.getPosition();
        if(!this.notMove)
        {
            if(this.stateposition == 1 && this.moveFinish)
            {
               this.moveLeft();
	           this.setPosition( new cc.Point( 700 , pos.y) );
               this.moveFinish = false;
	        }
        
            if(this.stateposition == 2 && this.moveFinish)
            {
               this.moveRight();
	           this.setPosition( new cc.Point( 100 , pos.y) );
               this.moveFinish = false;
	        }
        }
    },
    venonSceneVariable: function(){
        this.direction_y = 0;
        this.direction_x = 0;
        this.movingAction = this.createAnimationAction();
        this.isMovingLeft = false;
        this.isMovingRight = false;
        this.hitpoint = 300;
        this.stateposition = 1;
        this.moveFinish = true;
    },
    moveLeft: function(){
        this.setFlippedX(true);
        if(!this.isMovingLeft)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
        }
	    this.direction_x = Venon.DIR.LEFT;
        this.direction_y = 0;
    },
    
    moveRight: function() {
        this.setFlippedX(false);
        if(!this.isMovingRight)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
        }
	    this.direction_x = Venon.DIR.RIGHT;
        this.direction_y = Venon.DIR.DOWN;
    },
    
    setToDefault: function(){
        if(this.isMovingLeft)
        {
            this.isMovingLeft=false;
            this.stopAction( this.movingAction );
        }
        if(this.isMovingRight)
        {
            this.isMovingRight=false;
            this.stopAction( this.movingAction );
        }
        this.direction_x = 0;
        this.direction_y = 0;
    },
    
    knifehit: function(){
        if(this.hitpoint - 1 >= 0)
            this.hitpoint -= 1;
    },
    
    move: function(){
        this.moveFinish = true;
        if(this.stateposition == 1)
            this.stateposition = 2;
        else
            this.stateposition = 1;
    },
    
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Boss/Venon-R-1.png' );
        animation.addSpriteFrameWithFile( 'res/images/Boss/Venon-R-3.png' );
	    animation.setDelayPerUnit( 0.5 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    notMove: function(){
        return this.notMove = true;
    }
});

Venon.DIR = {
    LEFT: 0,
    RIGHT: 0
};