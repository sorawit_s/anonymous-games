var HowtoplayLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );  
        this.background = new HowtoplayBG();
        this.background.setPosition( new cc.Point( 400, 300 ) );
        this.addChild( this.background );
	    this.background.scheduleUpdate();
        this.addKeyboardHandlers();
        
	    return true;
    },
    update: function() {
       
    },
    onKeyDown: function( e ) {
        switch(e)
        {
            case cc.KEY.enter : case cc.KEY.escape :
                cc.audioEngine.stopAllEffects();
                this.soundEffect(1);
                cc.director.runScene(new cc.TransitionFade(1, new PlayMenuScene()));
                break;
        }
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            }
        }, this);
    },
    soundEffect: function( select ){
        var number = select;
        switch(number){
            case 1:
                cc.audioEngine.playEffect( 'res/sounds/taptap.mp3' );
                break;
        }
    }
});