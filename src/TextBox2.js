var TextBox2 = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/textbox.png' );
    },
    update: function() {
        
    },
    openingText: function(numberOfPassage) {
        var passage = '';
        var number = numberOfPassage;
        switch(number)
        {
          case 1 : passage = 'Great, you complete to collect\n\nthe knives.'; break;
          case 2 : passage = 'Now, you can use the ultimate\n\npower of them.'; break; 
          case 3 : passage = 'Let\'s throw them to him.'; break;  
        }
        return passage;
    },
    whoSpeak: function(numberOfPassage) {
        var name = '(';
        var number = numberOfPassage;
        switch(number)
        {
          case 1 : name += 'Goddess'; break;
          case 2 : name += 'Goddess'; break;            
          case 3 : name += 'Goddess'; break;
        }
        return name+')';
    }
});