var MainCharactor = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/MainCharactor-R-1.png' );
        this.movingAction = this.createAnimationAction();
        this.mainCharactorVariable();
    },
    
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/MainCharactor-R-3.png' );
	    animation.setDelayPerUnit( 0.2 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    update: function( dt ) {
	    var pos = this.getPosition();
        if(this.move)
        {
            if(pos.x<=40&&this.direction_x>0)
                this.setPosition( new cc.Point( pos.x + this.direction_x, pos.y + this.vy) );
            else if(pos.x>=770&&this.direction_x<0)
                this.setPosition( new cc.Point( pos.x + this.direction_x, pos.y + this.vy));
            else if(pos.x>=40&&pos.x<=770)                 
                this.setPosition( new cc.Point( pos.x + this.direction_x, pos.y + this.vy));              
        }
        else
        {
            this.setPosition( new cc.Point( pos.x, pos.y + this.vy) );
        }
        if(this.isJumping&&!this.isFalling){
            if(pos.y<=180)
            {
                if(!this.isSpecialMove)
                    this.vy += MainCharactor.DIR.UP;
                else
                    this.vy += MainCharactor.DIR.UP_SPECIAL;
            }
            else 
            {
                this.setPosition( new cc.Point( pos.x, 200) );
                this.isFalling = true;    
            }
        }
        else if(this.isFalling)
        {
            if(pos.y>=110)
            {
                if(!this.isSpecialMove)
                    this.vy += MainCharactor.DIR.DOWN;
                else
                    this.vy += MainCharactor.DIR.DOWN_SPECIAL;
            }
            else
            {
                this.vy = 0;
                this.setPosition( new cc.Point( pos.x, 70) );
                this.isFalling = false;
                this.isJumping = false;
            }
        }
    },
    
    mainCharactorVariable: function(){
        this.isMovingLeft = false;
        this.isMovingRight = false;
        this.isJumping = false;
        this.isFalling = false;
        this.move = false;
        this.isSpecialMove = false;
        this.direction_x = 0;
        this.vy = 0;
    },
    
    jump: function(){
        if(!this.isJumping)
        {
            this.soundEffect(1);
            this.isJumping = true;
        }
    },
    
    moveLeft: function(){
        this.setFlippedX(true);
        if(this.isMovingLeft==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
        }
        
        if(!this.isSpecialMove)
        {
	        this.direction_x = MainCharactor.DIR.LEFT;
        }
        else
        {
            this.direction_x = MainCharactor.DIR.LEFT_SPECIAL;
        }
    },
    
    moveRight: function() {
        this.setFlippedX(false);
        if(this.isMovingRight==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingRight = true;
        }
        
        if(!this.isSpecialMove)
        {
	        this.direction_x = MainCharactor.DIR.RIGHT;
        }
        else
        {
            this.direction_x = MainCharactor.DIR.RIGHT_SPECIAL;
        }
    },
    
    setToDefault: function(){
        if(this.isMovingLeft)
        {
            this.isMovingLeft=false;
            this.stopAction( this.movingAction );
        }
        if(this.isMovingRight)
        {
            this.isMovingRight=false;
            this.stopAction( this.movingAction );
        }
        this.direction_x = 0;
    },
    
    specialMoveActive: function(){
        this.isSpecialMove = true;
    },
    
    soundEffect: function( select ){
        var number = select;
        switch(number){
            case 1: 
                cc.audioEngine.playEffect( 'res/sounds/jump.mp3' );
                break;
        }
    }
});

MainCharactor.DIR = {
    UP: 2,
    DOWN: -2,
    LEFT: -4,
    RIGHT: 4,
    LEFT_SPECIAL: -3,
    RIGHT_SPECIAL: 3,
    UP_SPECIAL: 1,
    DOWN_SPECIAL: -1
};

MainCharactor.JUMPING_VELOCITY = 200;