var Knife6 = Knife.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/knives/blue.png' );
        this.direction = direction;
        this.isRemove = false;
        this.isSpecialMove = false;
    },
    
    update: function( dt ) {
        this.moveKnife();
    }
});