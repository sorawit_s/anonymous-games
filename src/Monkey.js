var Monkey = LittleRhyno.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/Monster/monkey-R-1.png' );
        this.monkeyVariable();
        
        this.step = 0;
        this.period = 30.0 + Math.random() * 20;
        this.speed = 0;
    },
    update: function( dt ) {
	    var pos = this.getPosition();
        this.step += 1;
        if(pos.x<700&&!(this.isMoveToRightEdge))
        {
            this.moveRight();
            this.speed = Monkey.MOVESPEED;
        }
        else
        {
            this.moveLeft();
            this.speed = -Monkey.MOVESPEED;
            this.isMoveToRightEdge = true;
            if(pos.x<150)
                this.isMoveToRightEdge = false;
        }      
        this.setPosition( new cc.Point( pos.x+this.speed , pos.y+ 4 * Math.sin( Math.PI * this.step / this.period ) ));
    },
    moveLeft: function(){
        this.setFlippedX(true);
        if(this.isMovingLeft==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = false;
            this.isMovingRight = false;
        }
    },
    moveRight: function() {
        this.setFlippedX(false);
        if(this.isMovingRight==false)
        {   
            this.runAction( this.movingAction );
            this.isMovingLeft = true;
            this.isMovingRight = false;
        }
    },
    monkeyVariable: function(){
        this.movingAction = this.createAnimationAction();
        this.isMoveToRightEdge = false;
        this.isMovingLeft = false;
        this.isMovingRight = false;
    },
    createAnimationAction: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'res/images/Monster/monkey-R-1.png' );
	    animation.addSpriteFrameWithFile( 'res/images/Monster/monkey-R-2.png' );
        animation.addSpriteFrameWithFile( 'res/images/Monster/monkey-R-3.png' );
	    animation.setDelayPerUnit( 0.5 );
	    return cc.RepeatForever.create( cc.Animate.create( animation ) );
    }
});

Monkey.MOVESPEED = 6;