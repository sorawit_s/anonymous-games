var TextBox = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/textbox.png' );
    },
    update: function() {
        
    },
    openingText: function(numberOfPassage) {
        var passage = '';
        var number = numberOfPassage;
        switch(number)
        {
          case 1 : passage = 'Welcome to my castle, White Knight'; break; //Goddess(1)
          case 2 : passage = 'Thank you'; break;                     
          case 3 : passage = 'Why you call me here?'; break;  
          case 4 : passage = 'The situation is not good.'; break;      
          case 5 : passage = 'How?'; break;                          
          case 6 : passage = 'Venon is coming'; break;               
          case 7 : passage = 'It\'s impossible. I already destroy him.'; break; 
          case 8 : passage = 'No, he\'s not dead. He\'s return.'; break;
          case 9 : passage = 'He\'s stronger and more powerfull.'; break;  
          case 10 : passage = 'You have to defeat him'; break;
          case 11 : passage = 'I can\'t do this. I used all of my power\n\n   to defeat him last time.'; break;       
          case 12 : passage = 'Don\'t worry. I have ever shared\n\n my power to protect the earth.'; break;
          case 13 : passage = 'The 7 knives'; break;
          case 14 : passage = 'But I don\'t have enough power\n\n to call all of them to you.'; break;
          case 15 : passage = 'Only 3 knives that I can give to you.'; break;
          case 16 : passage = 'You must to find other.';  break;
          case 17 : passage = 'You will know where is the knives \n\nwhen you get one of them'; break;
          case 18 : passage = 'Ok, I will do it.'; break;
          case 19 : passage = 'Let\'s go.'; break; 
          case 20 : passage = 'Good luck.'; break;
        }
        return passage;
    },
    whoSpeak: function(numberOfPassage) {
        var name = '(';
        var number = numberOfPassage;
        switch(number)
        {
          case 1 : name += 'Goddess'; break;
          case 2 : name += 'White knight'; break;            
          case 3 : name += 'White knight'; break;
          case 4 : name += 'Goddess'; break;
          case 5 : name += 'White knight'; break;
          case 6 : name += 'Goddess'; break;
          case 7 : name += 'White knight'; break;
          case 8 : name += 'Goodess'; break;
          case 9 : name += 'Goodess'; break;
          case 10 : name += 'Goodess'; break;
          case 11 : name += 'White knight'; break;
          case 12 : name += 'Goodess'; break;
          case 13 : name += 'Goddess'; break;
          case 14 : name += 'Goddess'; break;
          case 15 : name += 'Goodess'; break;
          case 16 : name += 'Goddess'; break;
          case 17 : name += 'Goddess'; break;
          case 18 : name += 'White knight'; break;
          case 19 : name += 'White knight'; break;
          case 20 : name += 'Goddess'; break;
        }
        return name+')';
    }
});