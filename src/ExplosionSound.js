var ExplosionSound = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        cc.audioEngine.stopAllEffects();
        cc.audioEngine.playEffect( 'res/sounds/explosion.mp3' );
    },
    update: function( dt ) {
    }
});