var Knife7 = Knife.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/knives/black.png' );
        this.direction = direction;
        this.isRemove = false;
        this.isSpecialMove = false;
    },
    
    update: function( dt ) {
        this.moveKnife();
    }
});