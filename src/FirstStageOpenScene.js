var FirstStageOpenScene = cc.LayerColor.extend({
    ctor: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( 'res/sounds/OpenStage.mp3' );
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.openstageBG = new OpenStage1BG();
        this.openstageBG.setPosition(400,300);
        this.addChild(this.openstageBG);
        this.runStageScene();
        this.scheduleUpdate();
    },
    
    update: function()
    {
        
    },
    
    runStageScene: function()
    {
        var delay=4000;                         //delay 4 seconds
        setTimeout(function(){
            cc.director.runScene(new cc.TransitionFade(1, new FirstStageScene()));
        },delay);
    }
    
});

var FirstStageScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new FirstStageLayer();
        layer.init();
        this.addChild( layer );
    }
});
