var res = {
    //LogoScene
    LogoBGMusic_mp3 : 'res/sounds/LogoSceneMusic.mp3',
    //MenuScene
    MenuBGMusic_mp3 : 'res/sounds/soundMenu.mp3',
    //OpenStoryScene
    OpenStoryBGMusic_mp3 : 'res/sounds/OpenStory.mp3',
    //OpenStageScene
    OpenStageBGMusic_mp3 : 'res/sounds/OpenStage.mp3',
    //ClearStageScene
    CloseStageBGMusic_mp3 : 'res/sounds/ClearStage.mp3',
    //StageOne
    StageOneBGMusic_mp3 : 'res/sounds/firstMap.mp3',
    //StageTwo
    StageTwoBGMusic_mp3 : 'res/sounds/secondMap.mp3',
    //StageThree
    StageThreeBGMusic_mp3 : 'res/sounds/thirdMap.mp3',
    //StageFour
    StageFourBGMusic_mp3 : 'res/sounds/forthMap.mp3',
    //StageFinal
    StageFinalBGMusic_mp3 : 'res/sounds/finalMap.mp3',
    //CreditScene
    CreditSceneBGMusic_mp3 : 'res/sounds/CreditScene.mp3'
};

var LogoScene_resources = [
    
    res.LogoBGMusic_mp3
    
];

var MenuScene_resources = [
    
    res.MenuBGMusic_mp3  
    
];

var OpenStoryScene_resources = [
    
    res.OpenStoryBGMusic_mp3
    
];

var OpenStageScene_resources = [
    
    res.OpenStageBGMusic_mp3
    
];

var ClearStageScene_resources = [
  
    res.CloseStageBGMusic_mp3
    
];

var StageOne_resources = [
    
    res.StageOneBGMusic_mp3
    
];

var StageTwo_resources = [
    
    res.StageTwoBGMusic_mp3
    
];

var StageThree_resources = [
    
    res.StageThreeBGMusic_mp3
    
];

var StageFour_resources = [
    
    res.StageFourBGMusic_mp3  
    
];

var StageFinal_resources = [
    
    res.StageFinalBGMusic_mp3
    
];

var CreditScene_resources = [
    
    res.CreditSceneBGMusic_mp3
    
];