var FireBall = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/fireball-medium.png' );
        console.log("fireball");
        this.direction = direction;
        this.isRemove = false;
        this.move = true;
    },
    update: function( dt ) {
        this.moveFireBall();
    },
    moveFireBall: function( ){
        var pos = this.getPosition();
        if(this.move)
        {
            if(this.direction=='right')
            {
                this.setPosition( new cc.Point( pos.x+4, pos.y ) );
                if(this.getPosition().x >= 700)
                {
                    this.isRemove = true;
                    this.removeFireball();
                }
            }
            else if(this.direction=='left')//left
            {
                this.setRotation(-180);
                this.setPosition( new cc.Point( pos.x-4, pos.y ) );
                if(this.getPosition().x <= 100)
                {
                    this.isRemove = true;
                    this.removeFireball();
                }
            }
        }
        else
        {
            this.setPosition( new cc.Point( pos.x, pos.y ) );
        }
    },
    
    removeFireball: function(){
        this.removeFromParentAndCleanup(true);
    },
    
    checkRemoveFireball: function(){
        return this.isRemove;
    },
    
    notMove: function(){
        return this.move = false;
    }
});