var Knife5 = Knife.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/knives/purple.png' );
        this.direction = direction;
        this.isRemove = false;
        this.isSpecialMove = false;
    },
    
    update: function( dt ) {
        this.moveKnife();
    }
});