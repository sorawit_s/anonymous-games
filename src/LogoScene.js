var LogoScene = cc.LayerColor.extend({
    init: function() {
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic( res.LogoBGMusic_mp3 );
        this._super( new cc.Color( 255, 255, 255, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.logo = new Logo();
        this.creationpoint = new CreationPoint();
        this.initAddChildLogoScene();
        this.logoSceneVariable();
        
        this.scheduleUpdate();
        
	    return true;
    },
    update: function() {
        if(this.logo.isMoveFinish&&!this.isAddCreationpoint)
        {
            this.addChild(this.creationpoint);
            this.isAddCreationpoint = true;
            var delay=4000;                     //่delay 4 seconds
            setTimeout(function(){
                //load resources
                cc.LoaderScene.preload(MenuScene_resources, function () {
                cc.audioEngine.stopMusic();
                cc.audioEngine.stopAllEffects();
	            cc.director.runScene(new cc.TransitionFade(1, new PlayMenuScene()));
                }, this);
            },delay);
        }
    },
    initAddChildLogoScene: function(){
        this.logo.setPosition( new cc.Point( 400, 280 ) );
        this.creationpoint.setPosition( new cc.Point( 400, 100 ) );
        this.addChild( this.logo );
	    this.logo.scheduleUpdate();
        this.creationpoint.scheduleUpdate();
    },
    logoSceneVariable: function(){    
        this.isAddCreationpoint = false;
    }
});

var PlayMenuScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new MenuLayer();
        layer.init();
        this.addChild( layer );
    }
});