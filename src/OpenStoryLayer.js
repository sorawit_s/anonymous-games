var OpenStoryLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.background = new BackGround();
	    this.background.setPosition( new cc.Point( 400, 300 ) );
	    this.mainCharactor = new MainCharctor();
        this.mainCharactor.setPosition( new cc.Point( 100, 70 ) );
        this.blueArmor = new BlueArmor();
	    this.blueArmor.setPosition( new cc.Point( 300, 90 ) );
        this.goddess = new Goddess();
	    this.goddess.setPosition( new cc.Point( 650, 200 ) );
        this.textbox = new TextBox();
	    this.textbox.setPosition( new cc.Point( 400, 480 ) );
        this.addChild( this.background );
	    this.addChild( this.mainCharactor );
        this.addChild( this.blueArmor );
        this.addChild( this.goddess );
        this.addChild( this.textbox );
	    this.background.scheduleUpdate();
        this.mainCharactor.scheduleUpdate();
        this.blueArmor.scheduleUpdate();
        this.goddess.scheduleUpdate();
        this.textbox.scheduleUpdate();
        this.addKeyboardHandlers();
        this.mainCharactorDirection = 'right';
        this.knifePosition = 0;        
        this.scheduleUpdate();
	    return true;
    },
    update: function() {
	    if ( this.checkKnifeHit( ) ) 
        {
	       console.log("hit");
	    }
    },
    onKeyDown: function( e ) {
        if ( e == 13 ) {
            console.log( "Text is here" );
        }
        if ( e == 32 ) {
           this.mainCharactor.jump();
        }
	    if ( e == 38 ) {
           this.mainCharactor.moveUp();
        }
        if ( e == 40 ) {
	       this.mainCharactor.moveDown();
	    }
        if ( e == 37 ) {
           this.mainCharactorDirection = 'left';
	       this.mainCharactor.moveLeft();
	    }
        if ( e == 39 ) {
           this.mainCharactorDirection = 'right';
	       this.mainCharactor.moveRight();
	    }
        if ( e == 90 ) { //press 'z'
            //this.createKnife();
        }
        if ( e == 88 ) { //press 'x'
            //this.createKnife2();
        }
        if ( e == 67 ) { //press 'c'
            //this.createKnife3();
        }
    },
    onKeyUp: function( e ) {
         if ( e == 13 ) {
            
         }
         if ( e == 32 ) {

         }
         if ( e == 38 ) {
	       this.mainCharactor.setToDefault();
         }
         if ( e == 40 ) {
	       this.mainCharactor.setToDefault();
	     }
         if ( e == 37 ) {
	       this.mainCharactor.setToDefault();
         }
         if ( e == 39 ) {
	       this.mainCharactor.setToDefault();
	     }
         if ( e == 90 ) { //press 'z'
           this.createKnife();
         }
         if ( e == 88 ) { //press 'x'
           this.createKnife2();
         }
         if ( e == 67 ) { //press 'c'
           this.createKnife3();
         }
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( e ) {
                 self.onKeyDown( e );
            },
            onKeyReleased: function( e ) {
                 self.onKeyUp( e );
            }
        }, this);
    },
    createKnife: function(){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife = new Knife('right');
	        this.knife.setPosition( new cc.Point( pos.x+20, pos.y-10 ) );
        }
        else    //left
        {
            this.knife = new Knife('left');
	        this.knife.setPosition( new cc.Point( pos.x-20, pos.y-10 ) );
        }
        this.addChild( this.knife );
        this.knifePosition = this.knife.position();
	    this.knife.scheduleUpdate();
    },
    createKnife2: function(){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife2 = new Knife2('right');
	        this.knife2.setPosition( new cc.Point( pos.x+20, pos.y-10 ) );
        }
        else    //left
        {
            this.knife2 = new Knife2('left');
	        this.knife2.setPosition( new cc.Point( pos.x-20, pos.y-10 ) );
        }
        this.addChild( this.knife2 );
        this.knifePosition = this.knife2.position();
	    this.knife2.scheduleUpdate();
    },
    createKnife3: function(){
        var pos = this.mainCharactor.getPosition();
        if(this.mainCharactorDirection == 'right')
        {
            this.knife3 = new Knife3('right');
	        this.knife3.setPosition( new cc.Point( pos.x+20, pos.y-10 ) );
        }
        else    //left
        {
            this.knife3 = new Knife3('left');
	        this.knife3.setPosition( new cc.Point( pos.x-20, pos.y-10 ) );
        }
        this.addChild( this.knife3 );
        this.knifePosition = this.knife3.position();
	    this.knife3.scheduleUpdate();
    },
    checkKnifeHit: function( ) {
	    var knifePos = this.knifePosition;
	    var bluePos = this.blueArmor.getPosition();
  	    return  ( Math.abs( bluePos.x - knifePos.x ) <= 20 );
    }
});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new OpenStoryLayer();
        layer.init();
        this.addChild( layer );
    }
});