var GameOverScene = cc.LayerColor.extend({
    ctor: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        this.gameoverBG = new GameoverBG();
        this.gameoverBG.setPosition(400,300);
        this.addChild(this.gameoverBG);
        cc.audioEngine.playMusic( 'res/sounds/Gameover.mp3' );
        this.runStageScene();
        this.scheduleUpdate();
    },
    
    update: function()
    {
        
    },
    
    runStageScene: function()
    {
        var delay=6000;                         //delay 5 seconds
        setTimeout(function(){
            cc.LoaderScene.preload(MenuScene_resources, function () {
                cc.audioEngine.stopMusic();
                cc.audioEngine.stopAllEffects();
	            cc.director.runScene(new cc.TransitionFade(1, new MenuLayerScene()));
            }, this);
        },delay);
    }
    
});

var MenuLayerScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new MenuLayer();
        layer.init();
        this.addChild( layer );
    }
});
