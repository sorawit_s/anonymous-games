var ForceShield = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/forceshield-R-1.png' );
        this.setFlippedX(true);
        console.log("fireball");
    },
    update: function( dt ) {
        var pos = this.getPosition();
        if(pos.x>=-20)
        {
            this.setPosition( new cc.Point( pos.x-2, pos.y ) );
        }
        if(pos.x<=-20)
        {
            this.removeFromParentAndCleanup(true);
        }
    },
});