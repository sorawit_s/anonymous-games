var Knife3 = Knife.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/knives/green.png' );
        this.direction = direction;
        this.isRemove = false;
        this.isSpecialMove = false;
    },
    
    update: function( dt ) {
        this.moveKnife();
    }
});