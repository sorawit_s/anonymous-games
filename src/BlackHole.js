var BlackHole = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/blackHole.png' );
        this.rotation = 0;
    },
    update: function( dt ) {
        this.setRotation( this.rotation );
        this.rotation += 0.3;
    }
});