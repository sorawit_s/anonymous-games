var EnergyBall = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/EnergyBall2.png' );
        this.direction = direction;
        this.isRemove = false;
    },
    update: function( dt ) {
        this.moveFireBall();
    },
    moveFireBall: function( ){
        var pos = this.getPosition();
        this.setPosition( new cc.Point( pos.x+3, pos.y ) );
        if(this.getPosition().x >= 650)
        {
            this.isRemove = true;
            this.removeEnergyball();
        }
    },  
    removeEnergyball: function(){
        this.removeFromParentAndCleanup(true);
    }
});