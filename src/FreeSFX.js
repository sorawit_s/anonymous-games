var FreeSFX = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/Logo/freeSFX.png' );
        this.isMoveFinish = false;
    },
    
    update: function( dt ) {
        this.moveLogo();
    },
    
    moveLogo: function( ){
        var pos = this.getPosition();
        if(pos.y < 700)
        {
            this.setPosition( new cc.Point( pos.x, pos.y+3 ) );
        }
        else
        {
//            this.isMoveFinish = true;
//            this.setPosition( new cc.Point( pos.x, pos.y ) );
            this.removeLogo();
        }
    },
    
    removeLogo: function(){
        this.removeFromParentAndCleanup(true);
    },
});