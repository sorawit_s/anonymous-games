var Logo = cc.Sprite.extend({
    ctor: function(direction) {
        this._super();
        this.initWithFile( 'res/images/Logo/Logo.png' );
        this.isMoveFinish = false;
    },
    
    update: function( dt ) {
        this.moveLogo();
    },
    
    moveLogo: function( ){
        var pos = this.getPosition();
        if(pos.y < 380)
        {
            this.setPosition( new cc.Point( pos.x, pos.y+1 ) );
        }
        else
        {
            this.isMoveFinish = true;
            this.setPosition( new cc.Point( pos.x, pos.y ) );
        }
    }
});